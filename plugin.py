from Components.ActionMap import HelpableActionMap
from Components.config import config
from Components.Sources.List import List
from Components.TimerSanityCheck import TimerSanityCheck
from enigma import eTimer, eServiceReference, eServiceReferenceFS
from Plugins.Plugin import PluginDescriptor
from RecordTimer import RecordTimerEntry, AFTEREVENT
from Screens.HelpMenu import HelpableScreen
from Screens.InfoBar import InfoBar
from Screens.InfoBarGenerics import InfoBarInstantRecord, isMoviePlayerInfoBar
from Screens.MessageBox import MessageBox
from Screens.Screen import Screen
from Screens.TimerEdit import TimerSanityConflict
from Screens.TimerEntry import TimerEntry, TimerLog
from Tools.Directories import resolveFilename, SCOPE_ACTIVE_SKIN, fileExists
from Tools.LoadPixmap import LoadPixmap
from time import localtime, time, strftime
from copy import copy


def InfoBarInstantRecord__init__(self, *args, **kwargs):
	baseInfoBarInstantRecord__init__(self, *args, **kwargs)
	self["InstantRecordActions"].actions["instantRecord"] = currentTimers.open
	for (actionmap, context, actions) in self.helpList:
		if context == "InfobarInstantRecord":
			for index, item in enumerate(actions):
				if item[0] == "instantRecord":
					actions[index] = (item[0], _("Current timers"))

baseInfoBarInstantRecord__init__ = InfoBarInstantRecord.__init__
InfoBarInstantRecord.__init__ = InfoBarInstantRecord__init__


class CurrentTimers(Screen, HelpableScreen):
	skin = """
		<screen position="center,center" size="950,300" title="Current timers" flags="wfNoBorder">
			<widget position="10, 5" size="80, 25" source="global.CurrentTime" render="Label" font="Regular; 20" valign="center" foregroundColor="white" backgroundColor="black" transparent="1">
				<convert type="ClockToText">Default</convert>
			</widget>
			<widget position="c, 5" size="c-10, 25" source="Title" render="Label" font="Named;22" halign="right" foregroundColor="white" backgroundColor="black" transparent="1"/>
			<eLabel position="10,35" size="e-20, 2" backgroundColor="darkgrey"/>
			<widget position="5,45" size="940,250" source="timers" render="Listbox" scrollbarMode="showOnDemand" transparent="1">
				<convert type="TemplatedMultiContent">
					{"template": [
							MultiContentEntryPixmapAlphaBlend(pos=(5, 2), size=( 20, 20), png=2),
							MultiContentEntryText(pos=( 30, 0), size=( 30, 25), text=3, flags=RT_VALIGN_CENTER),
							MultiContentEntryText(pos=( 60, 0), size=(300, 25), text=4, flags=RT_VALIGN_CENTER),
							MultiContentEntryText(pos=(380, 0), size=(215, 25), text=5, flags=RT_VALIGN_CENTER),
							MultiContentEntryText(pos=(600, 0), size=( 80, 25), text=6, flags=RT_HALIGN_RIGHT|RT_VALIGN_CENTER),
							MultiContentEntryText(pos=(680, 0), size=(100, 25), text=7, flags=RT_VALIGN_CENTER),
							MultiContentEntryText(pos=(790, 0), size=( 95, 25), text=8, flags=RT_HALIGN_RIGHT|RT_VALIGN_CENTER),
							MultiContentEntryText(pos=(885, 0), size=( 40, 25), text=9, flags=RT_VALIGN_CENTER),
						],
					 "fonts": [gFont("Regular", 20)],
					 "itemHeight": 25
					}
				</convert>
			</widget>
		</screen>
	"""

	def __init__(self, session):
		Screen.__init__(self, session)
		HelpableScreen.__init__(self)
		self.parent = self.session.current_dialog

		self.iconWait = LoadPixmap(resolveFilename(SCOPE_ACTIVE_SKIN, "icons/timer_wait.png"))
		self.iconRecording = LoadPixmap(resolveFilename(SCOPE_ACTIVE_SKIN, "icons/timer_rec.png"))
		self.iconDone = LoadPixmap(resolveFilename(SCOPE_ACTIVE_SKIN, "icons/timer_done.png"))
		self.iconZapped = LoadPixmap(resolveFilename(SCOPE_ACTIVE_SKIN, "icons/timer_zap.png"))
		self.iconDisabled = LoadPixmap(resolveFilename(SCOPE_ACTIVE_SKIN, "icons/timer_off.png"))
		self.iconFailed = LoadPixmap(resolveFilename(SCOPE_ACTIVE_SKIN, "icons/timer_failed.png"))

		self.pollTimer = eTimer()
		self.pollTimer.callback.append(self.onTimer)
		self.interval = 60
		self.pollTimer.start(self.interval * 1000)
		self.filling = False
		self.list = []
		self.fillTimerList()
		#self.list.extend([(None, None, self.iconRecording, "A", "A Really Long Program Name", "7mateHD Rockhampton", "22:22:22", " - 23:23:23", "1232 mins", " left"),
		#				  (None, None, self.iconDone,	   "B", "A Really Long Program Name", "PEACH Central QLD",   "12:22PM",  " - 12:23AM",   "120 mins", " ago"),
		#				  (None, None, self.iconDisabled,  "C", "A Really Long Program Name", "WIN HD Central QLD",  "22:22:22", " - 23:23:23",  "120 mins", " ago"),
		#				  (None, None, self.iconWait,	   "D", "A Really Long Program Name", "triple j Unearthed",  "12:22PM",  " - 12:23AM",   "120 mins", " till")])
		self["timers"] = List(self.list)
		self["timeactions"] = HelpableActionMap(self, "CurrentTimersActions", {
			"decEnd": (self.decEnd, _("Decrease end time")),
			"incEnd": (self.incEnd, _("Increase end time")),
			"decStartEnd": (self.decStartEnd, _("Decrease start and end times")),
			"incStartEnd": (self.incStartEnd, _("Increase start and end times")),
		}, prio=-1, description=_("Time functions"))
		self["controlactions"] = HelpableActionMap(self, "CurrentTimersActions", {
			"stop": (self.stop, _("Stop")),
			"stopAll": (self.stopAll, _("Stop all")),
			"delete": (self.timerDelete, _("Delete timer")),
			"edit": (self.timerEdit, _("Edit timer")),
			"toggle": (self.timerToggle, _("Toggle timer")),
			"log": (self.timerLog, _("Show timer log")),
		}, description=_("Control functions"))
		self["exitactions"] = HelpableActionMap(self, "CurrentTimersActions", {
			"exit": (self.close, _("Exit")),
			"zap": (self.zap, _("Zap to channel")),
			"timer": (self.exitToTimer, _("Show timers")),
			"media": (self.exitToMedia, _("Show movies")),
			"epg": (self.exitToEPG, _("Show EPG")),
			"play": (self.play, _("Play movie")),
		}, description=_("Exit functions"))

		self.changeTimer = eTimer()
		self.changeTimer.callback.append(self.afterChange)

	def fillTimerTest(self, timer):
		now = int(time())
		if timer.justplay:
			return 0 <= timer.begin - now <= 2 * 60 * 60
		return (timer.isRunning() or
				timer.begin <= now <= timer.end or
				0 <= timer.begin - now <= 2 * 60 * 60 or
				0 <= now - timer.end <= 2 * 60 * 60)

	def fillRepeatedTimerTest(self, timer):
		if timer.repeated:
			prev_timer = self.previousTimer(timer)
			if prev_timer:
				return self.fillTimerTest(prev_timer)
		return False

	def previousTimer(self, timer):
		for entry in reversed(timer.log_entries):
			if entry[1] == 12:	# stop
				prev_timer = copy(timer)
				dur = timer.end - timer.begin
				prev_timer.end = entry[0]
				# This is wrong if you stop it manually, but it's not
				# worth the effort to do it right.
				prev_timer.begin = prev_timer.end - dur
				prev_timer.state = prev_timer.StateEnded
				return prev_timer

	def fillTimerList(self):
		self.filling = True
		cur_ref = self.session.nav.getCurrentlyPlayingServiceReference()
		cur_ref = cur_ref and cur_ref.toString()
		def compare(x, y):
			if x[0].isRunning():
				if y[0].isRunning():
					if str(x[0].service_ref) == cur_ref:
						if str(y[0].service_ref) == cur_ref:
							return cmp(x[0].end, y[0].end)
						return -1
					if str(y[0].service_ref) == cur_ref:
						return +1
					return cmp(x[0].end, y[0].end)
				return -1
			if y[0].isRunning():
				return +1
			if x[0].state != y[0].state:
				return cmp(x[0].state, y[0].state)
			if x[0].state < x[0].StateRunning:
				return cmp(x[0].begin, y[0].begin)
			return cmp(y[0].end, x[0].end)

		list = self.list
		del list[:]
		list.extend([(timer, False) for timer in self.session.nav.RecordTimer.timer_list if self.fillTimerTest(timer)])
		list.extend([(self.previousTimer(timer), True) for timer in self.session.nav.RecordTimer.timer_list if self.fillRepeatedTimerTest(timer)])
		list.extend([(timer, True) for timer in self.session.nav.RecordTimer.processed_timers if self.fillTimerTest(timer)])
		list.sort(cmp=compare)
		interval = self.interval
		self.interval = 60
		self.list = [self.buildTimerEntry(timer[0], timer[1]) for timer in list]
		if interval != self.interval:
			self.pollTimer.changeInterval(self.interval * 1000)
		self.filling = False

	def onTimer(self):
		if not self.filling:
			self.refill()

	def refill(self):
		index = self["timers"].index
		if index is not None:
			timer = self.list[index][0]
		oldlen = len(self.list)
		self.fillTimerList()
		if index is not None and (index >= len(self.list) or self.list[index][0] != timer):
			for i, t in enumerate(self.list):
				if t[0] == timer:
					index = i
					break
		if oldlen == len(self.list):
			self["timers"].updateList(self.list)
		else:
			self["timers"].setList(self.list)
		if index is not None:
			self["timers"].index = index

	def buildTimerEntry(self, timer, processed):
		tuner = ""
		if timer.isRunning():
			feinfo = timer.record_service.frontendInfo()
			fedata = feinfo and feinfo.getFrontendData()
			tn = fedata and fedata.get("tuner_number")
			if tn >= 0:
				tuner = chr(ord('A') + tn)

		begin = strftime(config.usage.time.short.value, localtime(timer.begin))
		if timer.justplay:
			end = _(" ZAP")
		else:
			end = " - " + strftime(config.usage.time.short.value.replace("%-", "%_"), localtime(timer.end))

		icon = None
		if not processed:
			if timer.state in (timer.StateWaiting, timer.StatePrepared):
				icon = self.iconWait
			elif timer.state == timer.StateRunning:
				icon = self.iconRecording
			elif timer.state == timer.StateEnded:
				icon = self.iconDone
			elif timer.justplay:
				icon = self.iconZapped
		elif timer.disabled:
			icon = self.iconDisabled
		elif timer.failed:
			icon = self.iconFailed
		else:
			icon = self.iconDone

		now = int(time())
		if timer.begin > now:
			duration = timer.begin - now
			when = _("till")
		elif timer.end > now:
			duration = timer.end - now
			when = _("left")
		else:
			duration = now - timer.end
			when = _("ago")
		if duration > 60:
			duration /= 60
			if duration == 1:
				period = _("min")
			else:
				period = _("mins")
		else:
			self.interval = 1
			if duration == 1:
				period = _("sec")
			else:
				period = _("secs")

		return (timer, processed,
				icon, tuner, timer.name, timer.service_ref.getServiceName(),
				begin, end, "%d %s" % (duration, period), " " + when)

	def getCurrentTimer(self):
		cur = self["timers"].current
		return cur and cur[0]

	def zap(self):
		timer = self.getCurrentTimer()
		if not timer:
			return
		if isMoviePlayerInfoBar(self.parent):
			self.session.open(MessageBox, _("Zap not supported during playback."), MessageBox.TYPE_INFO, timeout=3)
		else:
			self.close(("zap", timer.service_ref.ref))

	# Ignore past timers.
	def getValidTimer(self):
		timer = self.getCurrentTimer()
		now = int(time())
		if not timer or timer.end < now:
			return None
		return timer

	def timerCheck(self, timer, new_begin, new_end):
		dummyentry = RecordTimerEntry(
			timer.service_ref, new_begin, new_end, timer.name, timer.description, timer.eit, disabled=True,
			justplay=timer.justplay, afterEvent=timer.afterEvent, dirname=timer.dirname, tags=timer.tags)
		dummyentry.disabled = timer.disabled
		if TimerSanityCheck(self.session.nav.RecordTimer.timer_list, dummyentry).check():
			timer.begin = new_begin
			timer.end = new_end
			self.refill()
			self.change_timer = timer
			self.changeTimer.start(2000, True)

	def afterChange(self):
		self.session.nav.RecordTimer.timeChanged(self.change_timer)
		if not self.filling:
			self.refill()

	def decEnd(self):
		timer = self.getValidTimer()
		now = int(time())
		# Leave a minute remaining.
		if not timer or timer.end - 60 < now:
			return
		timer.end -= 60
		self.refill()
		self.change_timer = timer
		self.changeTimer.start(2000, True)

	def incEnd(self):
		timer = self.getValidTimer()
		if not timer:
			return
		self.timerCheck(timer, timer.begin, timer.end + 60)

	def decStartEnd(self):
		timer = self.getValidTimer()
		if not timer:
			return
		if timer.isRunning():
			self.decEnd()
		else:
			now = int(time())
			shift = min(timer.begin - now, 60)
			self.timerCheck(timer, timer.begin - shift, timer.end - shift)

	def incStartEnd(self):
		timer = self.getValidTimer()
		if not timer:
			return
		if timer.isRunning():
			self.timerCheck(timer, timer.begin, timer.end + 60)
		else:
			self.timerCheck(timer, timer.begin + 60, timer.end + 60)

	def play(self):
		timer = self.getCurrentTimer()
		if not timer or timer.begin > int(time()):
			return
		if isMoviePlayerInfoBar(self.parent):
			self.doPlay(True)
		else:
			self.parent.checkTimeshiftRunning(self.doPlay)

	def doPlay(self, answer):
		if answer:
			timer = self.getCurrentTimer()
			filename = None
			if hasattr(timer, "Filename"):
				filename = timer.Filename
			else:
				for __, __, msg in reversed(timer.log_entries):
					# Filename calculated as: '%s'
					if msg.startswith("Filename calculated"):
						filename = msg[25:-1]
						break
			if filename:
				for ext in (".ts", ".stream"):
					recording = filename + ext
					if fileExists(recording):
						self.close(("play", recording))
						break
				else:
					self.session.open(MessageBox, _("Recording '%s' not found.") % filename, MessageBox.TYPE_INFO, timeout=10)
			else:
				self.session.open(MessageBox, _("Unable to determine file name."), MessageBox.TYPE_INFO, timeout=3)

	def stop(self):
		timer = self.getCurrentTimer()
		if not timer:
			return
		if timer.isRunning():
			self.stopTimer(timer)
			self.refill()

	def stopAll(self):
		for t in self.list:
			if t[0].isRunning():
				self.session.openWithCallback(self.stopAllCB, MessageBox, _("Do you really want to stop ALL recordings?"), default=False)
				break

	def stopAllCB(self, answer):
		if not answer:
			return
		for t in self.list:
			if t[0].isRunning():
				self.stopTimer(t[0])
		self.refill()

	def stopTimer(self, t):
		t.enable()
		t.processRepeated(findRunningEvent=False)
		self.session.nav.RecordTimer.doActivate(t)
		if t.repeated:
			self.session.nav.RecordTimer.timeChanged(t)

	def timerDelete(self):
		timer = self.getCurrentTimer()
		if not timer:
			return
		self.session.openWithCallback(self.removeTimer, MessageBox, _("Do you really want to delete '%s'?") % timer.name, default=False)

	def removeTimer(self, result):
		if not result:
			return
		timer = self["timers"].current[0]
		timer.afterEvent = AFTEREVENT.NONE
		self.session.nav.RecordTimer.removeEntry(timer)
		self.refill()

	def timerEdit(self):
		timer = self.getValidTimer()
		if not timer:
			return
		self.disable_on_cancel = False
		self.session.openWithCallback(self.finishedEdit, TimerEntry, timer)

	def finishedEdit(self, answer):
		if not answer[0]:
			if self.disable_on_cancel:
				answer[1].disable()
				self.session.nav.RecordTimer.timeChanged(answer[1])
				self.refill()
			return
		entry = answer[1]
		timersanitycheck = TimerSanityCheck(self.session.nav.RecordTimer.timer_list, entry)
		success = False
		if not timersanitycheck.check():
			simulTimerList = timersanitycheck.getSimulTimerList()
			if simulTimerList is not None:
				for x in simulTimerList:
					if x.setAutoincreaseEnd(entry):
						self.session.nav.RecordTimer.timeChanged(x)
				if not timersanitycheck.check():
					simulTimerList = timersanitycheck.getSimulTimerList()
					if simulTimerList is not None:
						self.session.openWithCallback(self.finishedEdit, TimerSanityConflict, simulTimerList)
				else:
					success = True
		else:
			success = True
		if success:
			self.session.nav.RecordTimer.timeChanged(entry)
			self.refill()

	def timerToggle(self):
		timer = self.getValidTimer()
		if not timer:
			return
		if timer.disabled:
			timersanitycheck = TimerSanityCheck(self.session.nav.RecordTimer.timer_list, timer)
			if timersanitycheck.doubleCheck():
				return
			timer.enable()
			if not timersanitycheck.check():
				simulTimerList = timersanitycheck.getSimulTimerList()
				if simulTimerList is not None:
					self.disable_on_cancel = True
					self.session.nav.RecordTimer.timeChanged(timer)
					self.session.openWithCallback(self.finishedEdit, TimerSanityConflict, simulTimerList)
					return
		else:
			if timer.isRunning():
				self.stopTimer(timer)
			timer.disable()
		self.session.nav.RecordTimer.timeChanged(timer)
		self.refill()

	def timerLog(self):
		timer = self.getCurrentTimer()
		if not timer:
			return
		self.session.open(TimerLog, timer)

	def exitToTimer(self):
		self.close("timer")

	def exitToMedia(self):
		self.close("media")

	def exitToEPG(self):
		self.close("epg")


class CurrentTimersMain():
	def __init__(self):
		self.session = None

	def gotSession(self, session):
		self.session = session

	def open(self):
		if self.session:
			self.session.openWithCallback(self.close, CurrentTimers)

	def close(self, answer=None):
		if not answer:
			return
		infobar = InfoBar.instance
		if isinstance(answer, str):
			if answer == "timer":
				infobar.timerSelection()
			elif answer == "media":
				infobar.showMovies()
			elif answer == "epg":
				infobar.showDefaultEPG()
		elif answer[0] == "play":
			# Provide the missing steps of InfoBar and MovieSelection.
			infobar.lastservice = self.session.nav.getCurrentlyPlayingServiceOrGroup()
			if infobar.lastservice and ':0:/' in infobar.lastservice.toString():
				infobar.lastservice = eServiceReference(config.movielist.curentlyplayingservice.value)
			if config.movielist.show_live_tv_in_movielist.value:
				if self.session.nav.getCurrentlyPlayingServiceReference():
					if ':0:/' not in self.session.nav.getCurrentlyPlayingServiceReference().toString():
						config.movielist.curentlyplayingservice.setValue(self.session.nav.getCurrentlyPlayingServiceReference().toString())
			self.session.nav.stopService()
			ref = eServiceReference(eServiceReference.idDVB, eServiceReference.noFlags, eServiceReferenceFS.file)
			ref.setPath(answer[1])
			infobar.movieSelected(ref)
		elif answer[0] == "zap":
			infobar.zapToService(answer[1], infobar.servicelist.getRoot())

currentTimers = CurrentTimersMain()


def sessionstart(reason, **kwargs):
	if reason == 0:
		currentTimers.gotSession(kwargs["session"])

def Plugins(**kwargs):
	return PluginDescriptor(where=[PluginDescriptor.WHERE_SESSIONSTART], fnc=sessionstart, needsRestart=True)
